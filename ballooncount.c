#include <stdlib.h>
#include <stdio.h>

enum balloon {
   B,
   A,
   L,
   O,
   N,
   COUNT
};

unsigned long balloon_count(const char * stream)
{
    char c;
    unsigned long balloon[COUNT] = {0};
    while ((c = *(stream++)) != '\0')
    {
        switch (c)
        {
            case 'b':
                balloon[B]++;
                break;
            case 'a':
                balloon[A]++;
                break;
            case 'l':
                balloon[L]++;
                break;
            case 'o':
                balloon[O]++;
                break;
            case 'n':
                balloon[N]++;
                break;

            default:
                break;
        }
    }

    /* Divide l's and o's by two to normalize */
    balloon[L] /= 2;
    balloon[O] /= 2;

    /* The smallest number now contains the number of "balloon"s */
    unsigned long min = (unsigned long) -1;
    for (int i = 0; i < COUNT; ++i)
    {
        if (balloon[i] < min)
            min = balloon[i];
    }

    return min;
}

int main(int argc, char *argv[])
{
    printf("balloon: %lu\n", balloon_count("asdfwenalloobballoon"));
    return EXIT_SUCCESS;
}
